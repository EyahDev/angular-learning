import {Component, Input, OnInit} from '@angular/core';
import {MachinesService} from "../services/machines.service";

@Component({
  selector: 'app-machine-item',
  templateUrl: './machine-item.component.html',
  styleUrls: ['./machine-item.component.scss']
})
export class MachineItemComponent implements OnInit {
  @Input() machine: any;

  constructor(private machinesServices: MachinesService) { }

  ngOnInit(): void {
  }

  getReverseLabel(machine) {
    return this.machinesServices.getReverseLabel(machine);
  }

  onSwitchOnOff() {
    this.machinesServices.switchOnOff(this.machine)
  }
}
