import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from "@angular/router";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MachinesListComponent } from './machines-list/machines-list.component';
import { MachineItemComponent } from './machine-item/machine-item.component';
import { MachineViewComponent } from './machine-view/machine-view.component';
import { AuthComponent } from './auth/auth.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MachineComponent } from './machine/machine.component';

import { MachinesService } from "./services/machines.service";
import {AuthService} from "./services/auth.service";
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import {AuthGuardService} from "./guard/auth-guard.service.";


const appRoutes: Routes = [
  {path: 'machine/:id', canActivate:[AuthGuardService], component: MachineComponent},
  {path: 'machines', canActivate:[AuthGuardService], component: MachinesListComponent},
  {path: 'auth', component: AuthComponent},
  {path: '', component: MachineViewComponent},
  {path: 'not-found', component: FourOhFourComponent},
  {path: '**', redirectTo: 'not-found'},
];

@NgModule({
  declarations: [
    AppComponent,
    MachinesListComponent,
    MachineItemComponent,
    AuthComponent,
    MachineViewComponent,
    NavbarComponent,
    MachineComponent,
    FourOhFourComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    MachinesService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
