import {Component} from '@angular/core';
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-learning';
  isAuth: boolean;

  constructor(private authService: AuthService) {}

  getAuthStatus() {
    return this.authService.isAuth;
  }
}
