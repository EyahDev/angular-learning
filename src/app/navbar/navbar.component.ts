import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit(): void {
  }

  onSignInSignOut() {
    this.authService.toggleSignInOut()
  }

  getReverseSign() {
    if (this.authService.isAuth) {
      return "Déconnexion";
    }

    return "Connexion";
  }
}
