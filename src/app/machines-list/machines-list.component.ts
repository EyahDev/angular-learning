import { Component, OnInit } from '@angular/core';
import {MachinesService} from "../services/machines.service";

@Component({
  selector: 'app-machines-list',
  templateUrl: './machines-list.component.html',
  styleUrls: ['./machines-list.component.scss']
})
export class MachinesListComponent implements OnInit {
  machines: any;
  buttonStatus: string;

  constructor(private machinesServices: MachinesService) { }

  ngOnInit(): void {
    this.machines = this.machinesServices.getMachines()
  }

  onSwitchOnAll() {
    this.machinesServices.switchOnAll();
  }

  onSwitchOffAll() {
    this.machinesServices.switchOffAll();
  }
}
