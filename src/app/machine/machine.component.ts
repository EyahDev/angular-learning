import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MachinesService} from "../services/machines.service";

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  machine : any;
  constructor(private route: ActivatedRoute,
              private machinesServices: MachinesService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.machine = this.machinesServices.getMachineById(+id);
  }

  getReverseLabel(machine) {
    return this.machinesServices.getReverseLabel(machine);
  }

  onSwitchOnOff() {
    this.machinesServices.switchOnOff(this.machine)
  }

}
