import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthService {
  isAuth = false;

  constructor(private router: Router) {
  }

  toggleSignInOut() {
    if (this.isAuth) {
      setTimeout(() => {
        this.isAuth = false;
        return this.router.navigate(['']);
      }, 750)
    } else {
      setTimeout(() => {
        this.isAuth = true;
      }, 2000)
    }
  }
}
