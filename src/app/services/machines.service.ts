export class MachinesService {
  machines = [
    {
      id: 1,
      name: "Télévision",
      status: "éteint"
    },
    {
      id: 2,
      name: "Machine à laver",
      status: "éteint"
    },
    {
      id: 3,
      name: "Xbox One X",
      status: "éteint"
    },
    {
      id: 4,
      name: "Nvidia Shield",
      status: "éteint"
    },
  ];

  getMachineById(id: number) {
    return this.machines.find((i) => {
      return i.id === id;
    });
  }

  getMachines() {
    return this.machines;
  }

  getReverseLabel(machine) {
    return machine.status === 'éteint' ? 'Allumer' : 'Eteindre';
  }

  switchOffAll() {
    for (let machine of this.machines) {
      machine.status = 'éteint';
    }
  }

  switchOnAll() {
    for (let machine of this.machines) {
      machine.status = 'allumé';
    }
  }

  switchOnOff(machine) {
    if (machine.status === 'éteint') {
      machine.status = 'allumé';
    } else {
      machine.status = 'éteint';
    }
  }
}
